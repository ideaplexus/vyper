# Anycubic Vyper Stealthburner Mod

## LED Effects for Klipper

Create a ssh connection to the Klipper host, then execute the following commands

```shell
cd ~
git clone https://github.com/julianschill/klipper-led_effect.git
cd klipper-led_effect
./install-led_effect.sh
```

Restart afterward the Klipper host.

## Klipper DGUS Display

Follow the documentation from the [repository](https://github.com/seho85/klipper-dgus).

```shell
cd ~
git clone https://github.com/seho85/klipper-dgus.git
cd dgus-klipper
./install_for_mainsailos.sh
```